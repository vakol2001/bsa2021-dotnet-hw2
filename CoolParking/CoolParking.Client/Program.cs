﻿using System.Threading.Tasks;

namespace CoolParking.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var app = new Application();
            await app.SetUp().Run();
        }
    }
}
