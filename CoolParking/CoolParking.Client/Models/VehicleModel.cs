﻿using CoolParking.BL.Models;

namespace CoolParking.Client.Models
{
    public class VehicleModel
    {
        public string Id { get; init; }
        public VehicleType VehicleType { get; init; }
        public decimal Balance { get; init; }
    }
}
