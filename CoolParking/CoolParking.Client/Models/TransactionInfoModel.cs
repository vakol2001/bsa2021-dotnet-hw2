﻿using System;

namespace CoolParking.Client.Models
{
    public struct TransactionInfoModel
    {
        public decimal Sum { get; init; }
        public string VehicleId { get; init; }
        public DateTime TransactionDate { get; init; }
        public override string ToString()
        {
            return $"[{TransactionDate.ToShortDateString(),10} {TransactionDate.ToLongTimeString(),8}] {Sum,6} c.u. was debited from the account of the {VehicleId,10} vehicle.";
        }
    }
}
