﻿using CoolParking.Client.Pages;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.Client
{
    class Application : IDisposable
    {
        public string ConetionString { get; private set; }
        public HttpClient Client { get; private set; }

        public Application SetUp()
        {
            Console.WriteLine("Enter the host to connect to, if left blank, https://localhost:5001/api/ will be used");
            var input = Console.ReadLine();
            ConetionString = string.IsNullOrEmpty(input) ? "https://localhost:5001/api/" : input;
            Client = new HttpClient
            {
                BaseAddress = new Uri(ConetionString)
            };
            return this;
        }
        public async Task Run()
        {
            await new MainPage(this).Run();
        }

        public void Dispose()
        {
            Client.Dispose();
        }
    }
}