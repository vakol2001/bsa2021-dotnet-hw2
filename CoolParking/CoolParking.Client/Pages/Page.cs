﻿using System;
using System.Globalization;
using System.Threading.Tasks;

namespace CoolParking.Client.Pages
{
    abstract class Page
    {
        protected readonly Application _app;

        protected Page(Application app)
        {
            _app = app;
        }

        public async Task Run()
        {
            while (true)
            {
                PrintVariants();
                int choice = ReadInt32(
                    "Enter your choice. It must be an integer from the list",
                    "Incorrect input. Please enter an integer");
                if (choice == 0)
                {
                    return;
                }
                await Choice(choice);
            }
        }

        protected static void PrintInvalidInput(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        protected static void PrintPleaseEnter(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        protected static int ReadInt32(string firstMessage, string valideteMessage)
        {
            PrintPleaseEnter(firstMessage);
            int input;
            while (!int.TryParse(Console.ReadLine(), out input))
            {
                PrintInvalidInput(valideteMessage);
            }
            return input;
        }

        protected static decimal ReadDecimal(string firstMessage, string valideteMessage)
        {
            PrintPleaseEnter(firstMessage);
            decimal input;
            while (!decimal.TryParse(Console.ReadLine(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out input))
            {
                PrintInvalidInput(valideteMessage);
            }
            return input;
        }

        protected static string ReadString(string firstMessage)
        {
            PrintPleaseEnter(firstMessage);
            return Console.ReadLine();
        }
        protected abstract Task Choice(int choice);
        protected abstract void PrintVariants();

        protected static void PrintSelected(string message)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write(message);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();
        }
    }
}
