﻿using System;
using System.Threading.Tasks;

namespace CoolParking.Client.Pages
{
    class MainPage : Page
    {
        public MainPage(Application app) : base(app)
        {
        }

        protected override async Task Choice(int choice)
        {
            switch (choice)
            {
                case 1:
                    await new ParkingInfoPage(_app).Run();
                    break;
                case 2:
                    await new TransactionInfoPage(_app).Run();
                    break;
                case 3:
                    await new VehiclePage(_app).Run();
                    break;

                default:
                    PrintInvalidInput("Invalid number");
                    break;
            }
        }

        protected override void PrintVariants()
        {
            PrintSelected("Parking management");
            Console.WriteLine("1 - Displaying information about parking (balance, free places, etc.)");
            Console.WriteLine("2 - Displaying information about transactions");
            Console.WriteLine("3 - Vehicle management (add, remove, etc.)");
            Console.WriteLine("0 - Exit");
        }
    }
}
