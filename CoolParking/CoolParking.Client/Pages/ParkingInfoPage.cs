﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace CoolParking.Client.Pages
{
    internal class ParkingInfoPage : Page
    {

        public ParkingInfoPage(Application app) : base(app)
        {
        }

        protected override async Task Choice(int choice)
        {
            switch (choice)
            {
                case 1:
                    Console.WriteLine($"Current parking balance: {await _app.Client.GetStringAsync("parking/balance")}");
                    break;
                case 2:
                    Console.WriteLine($"Amount of free places:  {await _app.Client.GetStringAsync("parking/freePlaces")}");
                    break;
                case 3:
                    var freePlaces = await _app.Client.GetStringAsync("parking/freePlaces");
                    var capacity = await _app.Client.GetStringAsync("parking/capacity");
                    Console.WriteLine($"Amount of occupied places: {int.Parse(capacity) - int.Parse(freePlaces)}");
                    break;
                case 4:
                    Console.WriteLine($"Parking capacity:  {await _app.Client.GetStringAsync("parking/capacity")}");
                    break;

                default:
                    PrintInvalidInput("Invalid number");
                    break;
            }
        }

        protected override void PrintVariants()
        {
            PrintSelected("Displaying information about parking");
            Console.WriteLine("1 - Current balance");
            Console.WriteLine("2 - Amount of free places");
            Console.WriteLine("3 - Amount of occupied places");
            Console.WriteLine("4 - Parking capacity");
            Console.WriteLine("0 - Back");
        }
    }
}