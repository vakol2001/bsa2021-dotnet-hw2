﻿using CoolParking.BL.Helpers;
using CoolParking.BL.Models;
using CoolParking.Client.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client.Pages
{
    internal class VehiclePage : Page
    {
        public VehiclePage(Application app) : base(app)
        {
        }

        protected override async Task Choice(int choice)
        {
            switch (choice)
            {
                case 1:
                    await PrintAllVehicles();
                    break;
                case 2:
                    await AddNewVehicle();
                    break;
                case 3:
                    await RemoveVehicle();
                    break;
                case 4:
                    await TopUpVehicle();
                    break;

                default:
                    PrintInvalidInput("Invalid number");
                    break;
            }
        }

        private async Task TopUpVehicle()
        {
            string id = ReadString("Enter the number of the vehicle you want to top up");
            decimal sum;
            while (true)
            {
                var input = ReadDecimal(
                    "Enter the top-up amount. Input format: dotted (integer_part.fractional_part)",
                    "Incorrect input. Please enter in correct format");
                if (input >= 0)
                {
                    sum = input;
                    break;
                }
                PrintInvalidInput("Negative top-up is not allowed");
            }
            var json = JsonConvert.SerializeObject(new { Id = id, Sum = sum });
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _app.Client.PutAsync($"transactions/topUpVehicle", data);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("The vehicle balance has been successfully replenished");
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                var responseString = await response.Content.ReadAsStringAsync();
                dynamic responseContent = JsonConvert.DeserializeObject(responseString);
                PrintInvalidInput(responseContent["title"].ToString());
                Console.WriteLine("Errors:");
                Console.WriteLine(responseContent["errors"].ToString());
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                PrintInvalidInput("There is no such vehicle at the parking");
            }
        }

        private async Task RemoveVehicle()
        {
            string id = ReadString("Enter the number of the vehicle you want to remove");

            var response = await _app.Client.DeleteAsync($"vehicles/{id}");
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Vehicle successfully removed");
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                PrintInvalidInput("There is no such vehicle at the parking");
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                var responseString = await response.Content.ReadAsStringAsync();
                dynamic responseContent = JsonConvert.DeserializeObject(responseString);
                PrintInvalidInput(responseContent["title"].ToString());
                Console.WriteLine("Errors:");
                Console.WriteLine(responseContent["errors"].ToString());
            }
        }

        private async Task AddNewVehicle()
        {
            string id = ReadString($"Enter the vehicle number. Must match regular expression {Vehicle.IdRedex}. Autogeneration - leave blank");
            if (id == string.Empty)
            {
                id = Vehicle.GenerateRandomRegistrationPlateNumber();
                Console.WriteLine($"Generated number {id}");
            }
            VehicleType type;
            foreach (var item in Enum.GetValues(typeof(VehicleType)))
            {
                Console.WriteLine($"{(int)item} - {item}");
            }
            while (true)
            {
                var input = ReadInt32(
                    "Enter the vehicle type number. Input format: integer",
                    "Incorrect input. Please enter in correct format");

                if (EnumHelper.IsDefined((VehicleType)input))
                {
                    type = (VehicleType)input;
                    break;
                }
                PrintInvalidInput("Invalid value");
            }
            decimal balance;
            while (true)
            {
                var input = ReadDecimal(
                    "Enter your starting balance. Input format: dotted (integer_part.fractional_part)",
                    "Incorrect input. Please enter in correct format");
                if (input >= 0)
                {
                    balance = input;
                    break;
                }
                PrintInvalidInput("Invalid negative balance");
            }
            var vehicle = new VehicleModel { Id = id, VehicleType = type, Balance = balance };
            var json = JsonConvert.SerializeObject(vehicle);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _app.Client.PostAsync("vehicles", data);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Vehicle added successfully");
            }
            else if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                var responseString = await response.Content.ReadAsStringAsync();
                dynamic responseContent = JsonConvert.DeserializeObject(responseString);
                PrintInvalidInput(responseContent["title"].ToString());
                Console.WriteLine("Errors:");
                Console.WriteLine(responseContent["errors"].ToString());
            }
        }

        private async Task PrintAllVehicles()
        {

            var vahiclesString = await _app.Client.GetStringAsync("vehicles");

            var vehicles = JsonConvert.DeserializeObject<IEnumerable<VehicleModel>>(vahiclesString);

            string numderTitle = "№", idTitle = "Number", typeTitle = "Type", balanceTitle = "Balance";
            var capacityString = await _app.Client.GetStringAsync("parking/capacity");
            int numberWidth = Math.Max(capacityString.Length, numderTitle.Length);
            int idWidth = vehicles.Select(v => v.Id.Length).Append(idTitle.Length).Max();
            int typeWidth = vehicles.Select(v => v.VehicleType.ToString().Length).Append(typeTitle.Length).Max();
            int balanceWidth = vehicles.Select(v => v.Balance.ToString().Length).Append(balanceTitle.Length).Max();

            string separateLine = $"+{new string('-', numberWidth + 2)}+{new string('-', idWidth + 2)}+{new string('-', typeWidth + 2)}+{new string('-', balanceWidth + 2)}+";
            Console.WriteLine(separateLine);
            string contentLine = "| {0," + numberWidth
                             + "} | {1," + idWidth
                             + "} | {2," + typeWidth
                             + "} | {3," + balanceWidth
                             + "} |";
            Console.WriteLine(contentLine, numderTitle, idTitle, typeTitle, balanceTitle);
            Console.WriteLine(separateLine);
            int index = 1;
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine(contentLine, index++, vehicle.Id, vehicle.VehicleType, vehicle.Balance);
                Console.WriteLine(separateLine);
            }
            int capacity = int.Parse(capacityString);
            while (index <= capacity)
            {
                Console.WriteLine(contentLine, index++, "", "", "");
                Console.WriteLine(separateLine);
            }
        }

        protected override void PrintVariants()
        {
            PrintSelected("Vehicle management");
            Console.WriteLine("1 - Show all vehicles");
            Console.WriteLine("2 - Add a new vehicle");
            Console.WriteLine("3 - Remove vehicle");
            Console.WriteLine("4 - Top up vehicle balance");
            Console.WriteLine("0 - Back");
        }
    }
}