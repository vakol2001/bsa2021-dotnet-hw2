﻿using CoolParking.BL.Models;
using CoolParking.Client.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace CoolParking.Client.Pages
{
    internal class TransactionInfoPage : Page
    {

        public TransactionInfoPage(Application app) : base(app)
        {
        }

        protected override async Task Choice(int choice)
        {
            switch (choice)
            {
                case 1:
                    await GetLastTransactions();
                    break;
                case 2:
                    await GetLastIncome();
                    break;
                case 3:
                    await GetAllTransactions();
                    break;

                default:
                    PrintInvalidInput("Invalid number");
                    break;
            }
        }

        private async Task GetAllTransactions()
        {
            Console.WriteLine("All transactions for the past periods: ");
            var response = await _app.Client.GetAsync("transactions/all");
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(await response.Content.ReadAsStringAsync());
                return;
            }
            PrintInvalidInput("Log file not found");
        }

        private async Task GetLastIncome()
        {
            var transactionsString = await _app.Client.GetStringAsync("transactions/last");
            var transactions = JsonConvert.DeserializeObject<IEnumerable<TransactionInfoModel>>(transactionsString);
            Console.WriteLine($"Income for the current period: {transactions.Sum(t => t.Sum)}");
        }

        private async Task GetLastTransactions()
        {
            Console.WriteLine("All transactions for the current period:");
            var transactionsString = await _app.Client.GetStringAsync("transactions/last");
            var transactions = JsonConvert.DeserializeObject<IEnumerable<TransactionInfoModel>>(transactionsString);
            foreach (var transaction in transactions)
            {
                Console.WriteLine(transaction.ToString());
            }
        }

        protected override void PrintVariants()
        {
            PrintSelected("Displaying information about transactions");
            Console.WriteLine("1 - All transactions for the current period");
            Console.WriteLine("2 - Income for the current period");
            Console.WriteLine("3 - All transactions for the past periods");
            Console.WriteLine("0 - Back");
        }
    }
}