﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Infrastructure.CoolParking.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCoolParking(this IServiceCollection services, IConfiguration configuration)
        {
            if (services is null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            if (configuration is null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            services.AddTransient<ILogService, LogService>(_ => new LogService(configuration["LogFilePath"]));
            services.AddTransient<ITimerService, TimerService>(_ => new TimerService(double.Parse(configuration["TimerInterval"])));
            services.AddSingleton<IParkingService, ParkingService>(p =>
            {
                var withdrawTimer = p.GetRequiredService<ITimerService>();
                withdrawTimer.Interval = Settings.ChargePeriod;
                var logTimer = p.GetRequiredService<ITimerService>();
                logTimer.Interval = Settings.LoggingPeriod;
                var logService = p.GetRequiredService<ILogService>();
                return new ParkingService(withdrawTimer, logTimer, logService);
            });
            return services;
        }
    }
}
