﻿using CoolParking.Application.Parking.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers.v1
{
    public class ParkingController : ApiController
    {
        public ParkingController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet("balance")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<decimal>> GetBalance()
        {
            return Ok(await Mediator.Send(new GetBalanceQuery()));
        }

        [HttpGet("capacity")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<int>> GetCapacity()
        {
            return Ok(await Mediator.Send(new GetCapacityQuery()));
        }

        [HttpGet("freePlaces")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<int>> GetFreePlaces()
        {
            return Ok(await Mediator.Send(new GetFreePlacesQuery()));
        }
    }
}
