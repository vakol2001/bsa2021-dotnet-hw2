﻿using CoolParking.Application.Transactions.Commands;
using CoolParking.Application.Transactions.Queries;
using CoolParking.BL.Models;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers.v1
{
    public class TransactionsController : ApiController
    {
        public TransactionsController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet("last")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<TransactionInfo>>> GetLastTransactions()
        {
            return Ok(await Mediator.Send(new GetLastTransactionsQuery()));
        }

        [HttpGet("all")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> GetAllTransactions()
        {
            var log = await Mediator.Send(new GetAllTransactionsQuery());
            if (log is null)
            {
                return NotFound();
            }
            return Ok(log);
        }

        [HttpPut("topUpVehicle")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Vehicle>> TopUpVehicle(TopUpVehicleCommand request)
        {
            var vehicle = await Mediator.Send(request);
            if (vehicle is null)
            {
                return NotFound();
            }
            return Ok(vehicle);
        }
    }
}
