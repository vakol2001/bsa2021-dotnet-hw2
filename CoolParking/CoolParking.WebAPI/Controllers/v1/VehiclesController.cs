﻿using CoolParking.Application.Vehicles.Commands;
using CoolParking.Application.Vehicles.Queries;
using CoolParking.BL.Models;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers.v1
{
    public class VehiclesController : ApiController
    {
        public VehiclesController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Vehicle>>> Get()
        {
            return Ok(await Mediator.Send(new GetAllVehiclesQuery()));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Vehicle>> GetById([FromRoute] GetVehicleByIdQuery request)
        {
            var vehicle = await Mediator.Send(request);
            if (vehicle is null)
            {
                return NotFound();
            }
            return Ok(vehicle);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Vehicle>> CreateVehicle(CreateVehicleCommand request)
        {
            return Created(new Uri($"{Request.Path}/{request.Id}", UriKind.Relative), await Mediator.Send(request));
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> DeleteVehicle([FromRoute] DeleteVehicleCommand request)
        {
            var result = await Mediator.Send(request);
            if (result.CannotDelete)
            {
                var modelState = new ModelStateDictionary();
                modelState.AddModelError("Vehicle", $"A vehicle with id {request.Id} has negative balance");
                var details = new ValidationProblemDetails(modelState);
                return BadRequest(details);
            }

            if (result.NotFound)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
