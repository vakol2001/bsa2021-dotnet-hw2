﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Interfase.Pages;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Interfase
{
    class Application
    {
        IParkingService _parkingService;
        ITimerService _withdrawTimer;
        ITimerService _logTimer;
        ILogService _logService;

        public IParkingService ParkingService { get => _parkingService; }

        public Application SetUp()
        {
            new SetUpPage(this).Run();
            _withdrawTimer = new TimerService(Settings.ChargePeriod);
            _logTimer = new TimerService(Settings.LoggingPeriod);
            _logService = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}/Transactions.log");
            return this;
        }
        public void Run()
        {
            using (_parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService))
            {
                new MainPage(this).Run();
            }

        }
    }
}
