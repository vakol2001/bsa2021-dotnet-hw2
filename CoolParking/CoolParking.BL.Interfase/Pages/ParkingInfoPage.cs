﻿using System;

namespace CoolParking.BL.Interfase.Pages
{
    internal class ParkingInfoPage : Page
    {

        public ParkingInfoPage(Application app) : base(app)
        {
        }

        protected override void Choice(int choice)
        {
            switch (choice)
            {
                case 1:
                    Console.WriteLine($"Текущий баланс парковки: {_app.ParkingService.GetBalance()}");
                    break;
                case 2:
                    Console.WriteLine($"Количество свободных мест: {_app.ParkingService.GetFreePlaces()}");
                    break;
                case 3:
                    Console.WriteLine($"Количество занятых мест: {_app.ParkingService.GetCapacity() - _app.ParkingService.GetFreePlaces()}");
                    break;
                case 4:
                    Console.WriteLine($"Размер парковки: {_app.ParkingService.GetCapacity()}");
                    break;

                default:
                    PrintInvalidInput("Недопустимое число");
                    break;
            }
        }

        protected override void PrintVariants()
        {
            PrintSelected("Вывод информации о парковке");
            Console.WriteLine("1 - Текущий баланс");
            Console.WriteLine("2 - Количество свободных мест");
            Console.WriteLine("3 - Количество занятых мест");
            Console.WriteLine("4 - Размер парковки");
            Console.WriteLine("0 - Назад");
        }
    }
}