﻿using System;
using System.Globalization;

namespace CoolParking.BL.Interfase.Pages
{
    abstract class Page
    {
        protected readonly Application _app;

        protected Page(Application app)
        {
            _app = app;
        }

        public void Run()
        {
            while (true)
            {
                PrintVariants();
                int choice = ReadInt32(
                    "Введите свой выбор. Это должно быть целое число из списка",
                    "Некорректный ввод. Введите целое число");
                if (choice == 0)
                {
                    return;
                }
                Choice(choice);
            }
        }

        protected static void PrintInvalidInput(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        protected static void PrintPleaseEnter(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        protected static int ReadInt32(string firstMessage, string valideteMessage)
        {
            PrintPleaseEnter(firstMessage);
            int input;
            while (!int.TryParse(Console.ReadLine(), out input))
            {
                PrintInvalidInput(valideteMessage);
            }
            return input;
        }

        protected static decimal ReadDecimal(string firstMessage, string valideteMessage)
        {
            PrintPleaseEnter(firstMessage);
            decimal input;
            while (!decimal.TryParse(Console.ReadLine(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out input))
            {
                PrintInvalidInput(valideteMessage);
            }
            return input;
        }

        protected static string ReadString(string firstMessage)
        {
            PrintPleaseEnter(firstMessage);
            return Console.ReadLine();
        }
        protected abstract void Choice(int choice);
        protected abstract void PrintVariants();

        protected static void PrintSelected(string message)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write(message);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();
        }
    }
}
