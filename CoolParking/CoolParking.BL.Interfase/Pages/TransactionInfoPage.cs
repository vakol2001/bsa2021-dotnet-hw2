﻿using System;
using System.Linq;

namespace CoolParking.BL.Interfase.Pages
{
    internal class TransactionInfoPage : Page
    {

        public TransactionInfoPage(Application app) : base(app)
        {
        }

        protected override void Choice(int choice)
        {
            switch (choice)
            {
                case 1:
                    Console.WriteLine("Все транзакции за текущий период:");
                    foreach (var transaction in _app.ParkingService.GetLastParkingTransactions())
                    {
                        Console.WriteLine(transaction.ToString());
                    }
                    break;
                case 2:
                    Console.WriteLine($"Заработок за текущий период: {_app.ParkingService.GetLastParkingTransactions().Sum(t => t.Sum)}");
                    break;
                case 3:
                    Console.WriteLine("Все транзакции за прошедшие периоды: ");
                    Console.WriteLine(_app.ParkingService.ReadFromLog());
                    break;

                default:
                    PrintInvalidInput("Недопустимое число");
                    break;
            }
        }

        protected override void PrintVariants()
        {
            PrintSelected("Вывод информации о транзакциях");
            Console.WriteLine("1 - Все транзакции за текущий период");
            Console.WriteLine("2 - Заработок за текущий период");
            Console.WriteLine("3 - Все транзакции за прошедшие периоды");
            Console.WriteLine("0 - Назад");
        }
    }
}