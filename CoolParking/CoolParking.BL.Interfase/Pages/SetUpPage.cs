﻿using CoolParking.BL.Models;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Interfase.Pages
{
    class SetUpPage : Page
    {
        public SetUpPage(Application app) : base(app)
        {
        }

        protected override void Choice(int choice)
        {
            switch (choice)
            {
                case 1:
                    ChangeIdRegex();
                    break;
                case 2:
                    ChangeParkingBalance();
                    break;
                case 3:
                    ChangeParkingSize();
                    break;
                case 4:
                    ChangeLoggingPeriod();
                    break;
                case 5:
                    ChangeChargePeriod();
                    break;
                case 6:
                    ChangePenaltyRate();
                    break;
                case 7:
                case 8:
                case 9:
                case 10:
                    ChangeTarifs(choice - 7); //minus first case
                    break;

                default:
                    PrintInvalidInput("Недопустимое число");
                    break;
            }
        }

        #region change methods
        private static void ChangePenaltyRate()
        {
            while (true)
            {
                var input = ReadDecimal(
                    "Введите новый коэффициент штрафа. Формат ввода: через точку (целая_часть.дробная_часть)",
                    "Некорректный ввод. Введите в правильном формате");
                if (input >= 0)
                {
                    Settings.PenaltyRate = input;
                    return;
                }
                PrintInvalidInput("Недопустимый отрицательный коэффициент");
            }
        }

        private static void ChangeTarifs(int choice)
        {
            while (true)
            {
                var input = ReadDecimal(
                    "Введите новый тариф. Формат ввода: через точку (целая_часть.дробная_часть)",
                    "Некорректный ввод. Введите в правильном формате");
                if (input >= 0)
                {
                    var retArr = Settings.Rates.ToArray();
                    Settings.Rates[retArr[choice].Key] = input;
                    return;
                }
                PrintInvalidInput("Недопустимый отрицательный тариф");
            }
        }

        private static void ChangeChargePeriod()
        {
            while (true)
            {
                var input = ReadInt32(
                    "Введите новый период оплаты. Формат ввода: челое число в секундах",
                    "Некорректный ввод. Введите в правильном формате");
                if (input >= 0)
                {
                    Settings.ChargePeriod = input * 1000.0;
                    return;
                }
                PrintInvalidInput("Недопустимый отрицательный период");
            }
        }

        private static void ChangeLoggingPeriod()
        {
            while (true)
            {
                var input = ReadInt32(
                    "Введите новый период логирования. Формат ввода: челое число в секундах",
                    "Некорректный ввод. Введите в правильном формате");
                if (input >= 0)
                {
                    Settings.LoggingPeriod = input * 1000.0;
                    return;
                }
                PrintInvalidInput("Недопустимый отрицательный период");
            }
        }

        private static void ChangeParkingSize()
        {
            while (true)
            {
                var input = ReadInt32(
                    "Введите новый размер парковки. Формат ввода: челое число",
                    "Некорректный ввод. Введите в правильном формате");
                if (input > 0)
                {
                    Settings.ParkingSize = input;
                    return;
                }
                PrintInvalidInput("Недопустимый неположительный размер");
            }
        }

        private static void ChangeParkingBalance()
        {
            Settings.ParkingBalance = ReadDecimal(
                "Введите новый начальный баланс парковки. Формат ввода: через точку (целая_часть.дробная_часть)",
                "Некорректный ввод. Введите в правильном формате");
        }

        private static void ChangeIdRegex()
        {
            try
            {
                Settings.IdRegex = ReadString("Введите новое регулярное выражение для номерного знака. Формат ввода: регулярное выражение");
            }
            catch (RegexParseException)
            {
                PrintInvalidInput("Введенная строка не может интерпритироваться как регулярное выражение");
            }
        }
        #endregion

        protected override void PrintVariants()
        {
            PrintSelected("Текущее состояние начальных параметров:");
            Console.WriteLine("Регулярное выражение для номеров машин: " + Settings.IdRegex);
            Console.WriteLine("Начальный баланс парковки: " + Settings.ParkingBalance + " у.е.");
            Console.WriteLine("Размер парковки: " + Settings.ParkingSize);
            Console.WriteLine("Период логирования: " + Settings.LoggingPeriod / 1000 + " с");
            Console.WriteLine("Период оплаты: " + Settings.ChargePeriod / 1000 + " с");
            Console.WriteLine("Коэффициент штрафа: " + Settings.PenaltyRate);
            foreach (var kvp in Settings.Rates)
            {
                Console.WriteLine($"Тариф на {kvp.Key}: {kvp.Value} у.е.");
            }
            PrintSelected("Меню:");
            Console.WriteLine("1 - Изменить регулярное выражение");
            Console.WriteLine("2 - Изменить баланс парковки");
            Console.WriteLine("3 - Изменить размер парковки");
            Console.WriteLine("4 - Изменить период логирования");
            Console.WriteLine("5 - Изменить период оплаты");
            Console.WriteLine("6 - Изменить коэффициент штрафа");
            int i = 7;
            foreach (var kvp in Settings.Rates)
            {
                Console.WriteLine($"{i++} - Изменить тариф для {kvp.Key}");
            }
            Console.WriteLine("0 - Далее");
        }
    }
}
