﻿using CoolParking.BL.Helpers;
using CoolParking.BL.Models;
using System;
using System.Linq;

namespace CoolParking.BL.Interfase.Pages
{
    internal class VehiclePage : Page
    {
        public VehiclePage(Application app) : base(app)
        {
        }

        protected override void Choice(int choice)
        {
            switch (choice)
            {
                case 1:
                    PrintAllVehicles();
                    break;
                case 2:
                    AddNewVehicle();
                    break;
                case 3:
                    RemoveVehicle();
                    break;
                case 4:
                    TopUpVehicle();
                    break;

                default:
                    PrintInvalidInput("Недопустимое число");
                    break;
            }
        }

        private void TopUpVehicle()
        {
            string id = ReadString("Введите номер транспортного средства, счет которого хотите пополнить");
            decimal sum;
            while (true)
            {
                var input = ReadDecimal(
                    "Введите сумму пополнения. Формат ввода: через точку (целая_часть.дробная_часть)",
                    "Некорректный ввод. Введите в правильном формате");
                if (input >= 0)
                {
                    sum = input;
                    break;
                }
                PrintInvalidInput("Недопустимо отрицательное пополнение");
            }
            try
            {
                _app.ParkingService.TopUpVehicle(id, sum);
            }
            catch (ArgumentException)
            {
                PrintInvalidInput("Такого транспортного средства на парковке нет");
            }
        }

        private void RemoveVehicle()
        {
            string id = ReadString("Введите номер транспортного средства, которое хотите удалить");
            try
            {
                _app.ParkingService.RemoveVehicle(id);
            }
            catch (ArgumentException)
            {
                PrintInvalidInput("Такого транспортного средства на парковке нет");
            }
            catch (InvalidOperationException)
            {
                PrintInvalidInput("Невозможно забрать транспортное средство с отрицательным балансом");
            }
        }

        private void AddNewVehicle()
        {
            string id = ReadString($"Введите номер транспортного средства. Должен соответствовать регулярному выражению {Vehicle.IdRedex}. Автогенигация - оставить пустым");
            if (id == string.Empty)
            {
                id = Vehicle.GenerateRandomRegistrationPlateNumber();
                Console.WriteLine($"Сгенирирован номер {id}");
            }
            VehicleType type;
            foreach (var item in Enum.GetValues(typeof(VehicleType)))
            {
                Console.WriteLine($"{(int)item} - {item}");
            }
            while (true)
            {
                var input = ReadInt32(
                    "Введите номер типа транспортного средства. Формат ввода: челое число",
                    "Некорректный ввод. Введите в правильном формате");

                if (EnumHelper.IsDefined((VehicleType)input))
                {
                    type = (VehicleType)input;
                    break;
                }
                PrintInvalidInput("Недопустимый значение");
            }
            decimal balance;
            while (true)
            {
                var input = ReadDecimal(
                    "Введите начальный баланс. Формат ввода: через точку (целая_часть.дробная_часть)",
                    "Некорректный ввод. Введите в правильном формате");
                if (input >= 0)
                {
                    balance = input;
                    break;
                }
                PrintInvalidInput("Недопустимый отрицательный баланс");
            }
            Vehicle vehicle;
            try
            {
                vehicle = new Vehicle(id, type, balance);
            }
            catch (ArgumentException)
            {
                PrintInvalidInput("Номер не соответствует регулярному выражению");
                return;
            }
            try
            {
                _app.ParkingService.AddVehicle(vehicle);
            }
            catch (ArgumentException)
            {
                PrintInvalidInput("Транспортное средство с таким номером уже на парковке");
            }
            catch (InvalidOperationException)
            {
                PrintInvalidInput("Парковка переполнена");
            }
        }

        private void PrintAllVehicles()
        {
            var vehicles = _app.ParkingService.GetVehicles();

            string numderTitle = "№", idTitle = "Номер", typeTitle = "Тип", balanceTitle = "Баланс";
            int numberWidth = Math.Max(_app.ParkingService.GetCapacity().ToString().Length, numderTitle.Length);
            int idWidth = vehicles.Select(v => v.Id.Length).Append(idTitle.Length).Max();
            int typeWidth = vehicles.Select(v => v.VehicleType.ToString().Length).Append(typeTitle.Length).Max();
            int balanceWidth = vehicles.Select(v => v.Balance.ToString().Length).Append(balanceTitle.Length).Max();

            string separateLine = $"+{new string('-', numberWidth + 2)}+{new string('-', idWidth + 2)}+{new string('-', typeWidth + 2)}+{new string('-', balanceWidth + 2)}+";
            Console.WriteLine(separateLine);
            string contentLine = "| {0," + numberWidth
                             + "} | {1," + idWidth
                             + "} | {2," + typeWidth
                             + "} | {3," + balanceWidth
                             + "} |";
            Console.WriteLine(contentLine, numderTitle, idTitle, typeTitle, balanceTitle);
            Console.WriteLine(separateLine);
            int index = 1;
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine(contentLine, index++, vehicle.Id, vehicle.VehicleType, vehicle.Balance);
                Console.WriteLine(separateLine);
            }
            while (index <= _app.ParkingService.GetCapacity())
            {
                Console.WriteLine(contentLine, index++, "", "", "");
                Console.WriteLine(separateLine);
            }
        }

        protected override void PrintVariants()
        {
            PrintSelected("Управление транспортными средствами");
            Console.WriteLine("1 - Показать все траспортные средства");
            Console.WriteLine("2 - Поставить новое транспортное средство");
            Console.WriteLine("3 - Забрать транспортное средство");
            Console.WriteLine("4 - Пополнить баланс транспортного средства");
            Console.WriteLine("0 - Назад");
        }
    }
}