﻿using System;

namespace CoolParking.BL.Interfase.Pages
{
    class MainPage : Page
    {
        public MainPage(Application app) : base(app)
        {
        }

        protected override void Choice(int choice)
        {
            switch (choice)
            {
                case 1:
                    new ParkingInfoPage(_app).Run();
                    break;
                case 2:
                    new TransactionInfoPage(_app).Run();
                    break;
                case 3:
                    new VehiclePage(_app).Run();
                    break;

                default:
                    PrintInvalidInput("Недопустимое число");
                    break;
            }
        }

        protected override void PrintVariants()
        {
            PrintSelected("Управление парковкой");
            Console.WriteLine("1 - Вывод информации о парковке (баланс, свободные места и т. д.)");
            Console.WriteLine("2 - Вывод информации о транзакциях");
            Console.WriteLine("3 - Управление транспортными средствами (поставить, забрать и т. д.)");
            Console.WriteLine("0 - Выход");
        }
    }
}
