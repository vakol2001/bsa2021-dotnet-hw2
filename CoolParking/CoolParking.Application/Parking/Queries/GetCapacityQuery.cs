﻿using CoolParking.BL.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Parking.Queries
{
    public class GetCapacityQuery : IRequest<int>
    {
        public class Hendler : IRequestHandler<GetCapacityQuery, int>
        {
            readonly IParkingService _parkingService;

            public Hendler(IParkingService parkingService)
            {
                _parkingService = parkingService;
            }

            public Task<int> Handle(GetCapacityQuery request, CancellationToken cancellationToken)
            {
                return Task.Run(() => _parkingService.GetCapacity());
            }
        }
    }
}
