﻿using CoolParking.BL.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Parking.Queries
{
    public class GetBalanceQuery : IRequest<decimal>
    {
        public class Hendler : IRequestHandler<GetBalanceQuery, decimal>
        {
            readonly IParkingService _parkingService;

            public Hendler(IParkingService parkingService)
            {
                _parkingService = parkingService;
            }

            public Task<decimal> Handle(GetBalanceQuery request, CancellationToken cancellationToken)
            {
                return Task.Run(() => _parkingService.GetBalance());
            }
        }
    }
}
