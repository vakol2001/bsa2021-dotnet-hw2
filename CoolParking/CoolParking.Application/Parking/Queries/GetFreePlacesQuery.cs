﻿using CoolParking.BL.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Parking.Queries
{
    public class GetFreePlacesQuery : IRequest<int>
    {
        public class Hendler : IRequestHandler<GetFreePlacesQuery, int>
        {
            readonly IParkingService _parkingService;
            public Hendler(IParkingService parkingService)
            {
                _parkingService = parkingService;
            }

            public Task<int> Handle(GetFreePlacesQuery request, CancellationToken cancellationToken)
            {
                return Task.Run(() => _parkingService.GetFreePlaces());
            }
        }

    }
}
