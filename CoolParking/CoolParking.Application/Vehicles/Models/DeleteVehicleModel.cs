﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Application.Vehicles.Models
{
    public class DeleteVehicleModel
    {
        public bool NotFound { get; init; } = false;
        public bool CannotDelete { get; init; } = false; 
    }
}
