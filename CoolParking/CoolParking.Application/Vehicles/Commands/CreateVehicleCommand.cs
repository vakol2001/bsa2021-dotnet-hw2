﻿using CoolParking.BL.Helpers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Vehicles.Commands
{
    public class CreateVehicleCommand : IRequest<Vehicle>
    {
        [BindProperty(Name = "id")]
        public string Id { get; init; }
        [BindProperty(Name = "vehicleType")]
        public VehicleType VehicleType { get; init; }
        [BindProperty(Name = "balance")]
        public decimal Balance { get; init; }

        public class Validator : AbstractValidator<CreateVehicleCommand>
        {
            public Validator(IParkingService parkingService)
            {
                RuleFor(x => x.Id).Cascade(CascadeMode.Stop)
                    .NotNull()
                    .Matches(Settings.IdRegex)
                    .WithMessage($"Id does not match regex {Settings.IdRegex}")
                    .Must(id => !parkingService.GetVehicles().Any(v => v.Id == id))
                    .WithMessage(x => $"A vehicle with id {x.Id} already exists");
                RuleFor(x => x)
                    .Configure(conf => conf.PropertyName = "freePlaces")
                    .Must(_ => parkingService.GetFreePlaces() > 0)
                    .WithMessage("There are no free places on parking");
                RuleFor(x => x.VehicleType)
                    .Must(vehicleType => EnumHelper.IsDefined(vehicleType))
                    .WithMessage("Such vehicle type is not defined");
                RuleFor(x => x.Balance)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Balance must be greater then or equal to 0");
            }
        }

        public class Hendler : IRequestHandler<CreateVehicleCommand, Vehicle>
        {
            readonly IParkingService _parkingService;

            public Hendler(IParkingService parkingService)
            {
                _parkingService = parkingService;
            }
            public Task<Vehicle> Handle(CreateVehicleCommand request, CancellationToken cancellationToken)
            {
                return Task.Run(() =>
                {
                    var vehicle = new Vehicle(request.Id, request.VehicleType, request.Balance);
                    _parkingService.AddVehicle(vehicle);
                    return vehicle;
                });
            }
        }

    }
}
