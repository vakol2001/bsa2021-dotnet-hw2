﻿using CoolParking.Application.Vehicles.Models;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Vehicles.Commands
{
    public class DeleteVehicleCommand : IRequest<DeleteVehicleModel>
    {
        [BindProperty(Name = "id", SupportsGet = true)]
        public string Id { get; init; }

        public class Validator : AbstractValidator<DeleteVehicleCommand>
        {
            public Validator()
            {
                RuleFor(x => x.Id)
                    .NotNull()
                    .Must(id => Regex.IsMatch(id, Settings.IdRegex))
                    .WithMessage($"Id does not match regex {Settings.IdRegex}");
            }
        }
        public class Hendler : IRequestHandler<DeleteVehicleCommand, DeleteVehicleModel>
        {
            readonly IParkingService _parkingService;

            public Hendler(IParkingService parkingService)
            {
                _parkingService = parkingService;
            }
            public Task<DeleteVehicleModel> Handle(DeleteVehicleCommand request, CancellationToken cancellationToken)
            {
                return Task.Run(()=> 
                {
                    DeleteVehicleModel model = new();
                    try
                    {
                        _parkingService.RemoveVehicle(request.Id);
                    }
                    catch (ArgumentException)
                    {
                        model = new DeleteVehicleModel { NotFound = true };
                    }
                    catch (InvalidOperationException)
                    {
                        model = new DeleteVehicleModel { CannotDelete = true };
                    }
                    return model;
                });
            }
        }
    }
}
