﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Vehicles.Queries
{
    public class GetAllVehiclesQuery : IRequest<IEnumerable<Vehicle>>
    {
        public class Hendler : IRequestHandler<GetAllVehiclesQuery, IEnumerable<Vehicle>>
        {
            readonly IParkingService _parkingService;

            public Hendler(IParkingService parkingService)
            {
                _parkingService = parkingService;
            }
            public async Task<IEnumerable<Vehicle>> Handle(GetAllVehiclesQuery request, CancellationToken cancellationToken)
            {
                return await Task.Run(() => _parkingService.GetVehicles());
            }
        }
    }
}
