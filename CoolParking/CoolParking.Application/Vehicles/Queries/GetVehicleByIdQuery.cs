﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Vehicles.Queries
{
    public class GetVehicleByIdQuery : IRequest<Vehicle>
    {
        [BindProperty(Name = "id", SupportsGet = true)]
        public string Id { get; init; }

        public class Validator : AbstractValidator<GetVehicleByIdQuery>
        {
            public Validator()
            {
                RuleFor(x => x.Id)
                    .NotNull()
                    .Must(id => Regex.IsMatch(id ,Settings.IdRegex))
                    .WithMessage($"Id does not match regex {Settings.IdRegex}");
            }
        }

        public class Hendler : IRequestHandler<GetVehicleByIdQuery, Vehicle>
        {
            readonly IParkingService _parkingService;

            public Hendler(IParkingService parkingService)
            {
                _parkingService = parkingService;
            }

            public Task<Vehicle> Handle(GetVehicleByIdQuery request, CancellationToken cancellationToken)
            {
                return Task.Run(() => _parkingService.GetVehicles().FirstOrDefault(v => v.Id == request.Id));
            }
        }
    }
}
