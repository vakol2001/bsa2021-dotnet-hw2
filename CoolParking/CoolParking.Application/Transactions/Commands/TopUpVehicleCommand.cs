﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Transactions.Commands
{
    public class TopUpVehicleCommand : IRequest<Vehicle>
    {
        [BindProperty(Name = "id")]
        public string Id { get; init; }
        [BindProperty(Name = "sum")]
        public decimal Sum { get; init; }

        public class Validator : AbstractValidator<TopUpVehicleCommand>
        {
            public Validator()
            {
                RuleFor(x => x.Id).Cascade(CascadeMode.Stop)
                   .NotNull()
                   .Matches(Settings.IdRegex)
                   .WithMessage($"Id does not match regex {Settings.IdRegex}");
                RuleFor(x => x.Sum)
                    .GreaterThanOrEqualTo(0)
                    .WithMessage("Sum must be greater then or equal to 0");
            }
        }

        public class Hendler : IRequestHandler<TopUpVehicleCommand, Vehicle>
        {
            readonly IParkingService _parkingService;
            public Hendler(IParkingService parkingService)
            {
                _parkingService = parkingService;
            }
            public Task<Vehicle> Handle(TopUpVehicleCommand request, CancellationToken cancellationToken)
            {
                return Task.Run(() => 
                {
                    var vehicle = _parkingService.GetVehicles().SingleOrDefault(v => v.Id == request.Id);
                    if (vehicle is not null)
                    {
                        _parkingService.TopUpVehicle(request.Id, request.Sum);
                    }
                    return vehicle;
                });
            }
        }
    }
}
