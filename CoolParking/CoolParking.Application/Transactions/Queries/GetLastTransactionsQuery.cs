﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Transactions.Queries
{
    public class GetLastTransactionsQuery : IRequest<IEnumerable<TransactionInfo>>
    {
        public class Hendler : IRequestHandler<GetLastTransactionsQuery, IEnumerable<TransactionInfo>>
        {
            readonly IParkingService _parkingService;

            public Hendler(IParkingService parkingService)
            {
                _parkingService = parkingService;
            }
            public async Task<IEnumerable<TransactionInfo>> Handle(GetLastTransactionsQuery request, CancellationToken cancellationToken)
            {
                return await Task.Run(() => _parkingService.GetLastParkingTransactions());
            }
        }
    }
}
