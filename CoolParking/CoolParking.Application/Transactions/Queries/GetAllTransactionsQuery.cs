﻿using CoolParking.BL.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.Application.Transactions.Queries
{
    public class GetAllTransactionsQuery : IRequest<string>
    {
        public class Hendler : IRequestHandler<GetAllTransactionsQuery, string>
        {
            readonly IParkingService _parkingService;
            public Hendler(IParkingService parkingService)
            {
                _parkingService = parkingService;
            }
            public Task<string> Handle(GetAllTransactionsQuery request, CancellationToken cancellationToken)
            {
                return Task.Run(() =>
                {
                    string log;
                    try
                    {
                        log = _parkingService.ReadFromLog();
                    }
                    catch (InvalidOperationException)
                    {
                        return null;
                    }
                    return log;
                });
            }
        }
    }
}
