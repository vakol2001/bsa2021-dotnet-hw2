﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        #region fields
        readonly Parking _parking = Parking.Instance;
        readonly ITimerService _withdrawTimer;
        readonly ITimerService _logTimer;
        readonly ILogService _logService;
        readonly IDictionary<VehicleType, decimal> _rates;
        readonly decimal _penaltyRate;
        private readonly List<TransactionInfo> _transactions = new();
        #endregion

        #region constructor and dispose

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer ?? throw new ArgumentNullException(nameof(withdrawTimer));
            _logTimer = logTimer ?? throw new ArgumentNullException(nameof(logTimer));
            _logService = logService ?? throw new ArgumentNullException(nameof(logService));
            _rates = Settings.Rates;
            _penaltyRate = Settings.PenaltyRate;
            _logTimer.Elapsed += LogTimer_Elapsed;
            _withdrawTimer.Elapsed += WithdrawTimer_Elapsed;
            _logTimer.Start();
            _withdrawTimer.Start();
        }
        public void Dispose()
        {
            _withdrawTimer.Dispose();
            _logTimer.Dispose();
            _parking.Vehicles.Clear();
            _parking.Balance = Settings.ParkingBalance;
        }
        #endregion

        #region getters fron _parking
        public decimal GetBalance() => _parking.Balance;

        public int GetCapacity() => _parking.Capasity;

        public int GetFreePlaces() => _parking.Capasity - _parking.Vehicles.Count;

        public ReadOnlyCollection<Vehicle> GetVehicles() => new(_parking.Vehicles);
        #endregion

        #region remove add
        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle is null)
            {
                throw new ArgumentNullException(nameof(vehicle));
            }

            if (_parking.Vehicles.Count == _parking.Capasity)
            {
                throw new InvalidOperationException("cannot add a vehicle on full parking");
            }
            if (_parking.Vehicles.Any(v => v.Id == vehicle.Id))
            {
                throw new ArgumentException($"A vehicle with id {vehicle.Id} already exists", nameof(vehicle));
            }
            _parking.Vehicles.Add(vehicle);
        }
        public void RemoveVehicle(string vehicleId)
        {
            if (string.IsNullOrEmpty(vehicleId))
            {
                throw new ArgumentException($"\"{nameof(vehicleId)}\" can't be null or empty.", nameof(vehicleId));
            }
            Vehicle vehicle;
            try
            {
                vehicle = _parking.Vehicles.Single(v => v.Id == vehicleId);
            }
            catch (InvalidOperationException e)
            {
                throw new ArgumentException($"A vehicle with id {vehicleId} is not exists", nameof(vehicleId), e);
            }
            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException($"A vehicle with id {vehicleId} have negative balance");
            }
            _parking.Vehicles.Remove(vehicle);
        }
        #endregion

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (string.IsNullOrEmpty(vehicleId))
            {
                throw new ArgumentException($"\"{nameof(vehicleId)}\" can't be null or empty.", nameof(vehicleId));
            }
            if (sum < 0)
            {
                throw new ArgumentException($"{nameof(sum)} cannot be negative", nameof(sum));
            }
            Vehicle vehicle;
            try
            {
                vehicle = _parking.Vehicles.Single(v => v.Id == vehicleId);
            }
            catch (InvalidOperationException e)
            {
                throw new ArgumentException($"A vehicle with id {vehicleId} is not exists", nameof(vehicleId), e);
            }
            vehicle.Balance += sum;
        }

        public string ReadFromLog() => _logService.Read();

        public TransactionInfo[] GetLastParkingTransactions() => _transactions.ToArray();
        private void TrunsferFunds(Vehicle vehicle, decimal sum)
        {
            if (vehicle is null)
            {
                throw new ArgumentNullException(nameof(vehicle));
            }
            decimal debt;
            if (vehicle.Balance >= sum)
            {
                debt = sum;
            }
            else if (vehicle.Balance > 0)
            {
                debt = (sum - vehicle.Balance) * _penaltyRate + vehicle.Balance;
            }
            else
            {
                debt = sum * _penaltyRate;
            }

            _parking.Balance += debt;
            vehicle.Balance -= debt;
            _transactions.Add(new TransactionInfo(debt, vehicle.Id));
        }


        private void WithdrawTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in _parking.Vehicles)
            {
                decimal sum = _rates[vehicle.VehicleType];
                TrunsferFunds(vehicle, sum);
            }
        }

        private void LogTimer_Elapsed(object sender, ElapsedEventArgs e)
        {

            if (_transactions.Count != 0)
            {
                var stringBuider = new StringBuilder();
                _transactions.Aggregate(stringBuider, (sb, t) => sb.AppendLine(t.ToString()));
                stringBuider.Remove(stringBuider.Length - 1, 1);
                _logService.Write(stringBuider.ToString());
                _transactions.Clear();
            }
            else
            {
                _logService.Write($"[{DateTime.Now.ToShortDateString(),10} {DateTime.Now.ToLongTimeString(),8}] There are no new transactions.");
            }
        }
    }
}