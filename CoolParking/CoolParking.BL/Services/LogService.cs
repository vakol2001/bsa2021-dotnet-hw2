﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string logFilePath)
        {
            if (string.IsNullOrEmpty(logFilePath))
            {
                throw new System.ArgumentException($"\"{nameof(logFilePath)}\" cannot be null or empty.", nameof(logFilePath));
            }
            LogPath = logFilePath;
        }

        public string LogPath { get; }

        public string Read()
        {
            try
            {
                return File.ReadAllText(LogPath);
            }
            catch (FileNotFoundException e)
            {
                throw new InvalidOperationException("Log file not found", e);
            }
            catch (DirectoryNotFoundException e)
            {
                throw new InvalidOperationException("Directory with log file not found", e);
            }
        }

        public void Write(string logInfo)
        {
            File.AppendAllText(LogPath, logInfo + '\n');
        }
    }
}