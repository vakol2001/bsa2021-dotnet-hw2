﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private readonly Timer _timer;

        public double Interval { get => _timer.Interval; set => _timer.Interval = value; }

        public TimerService(double interval)
        {
            _timer = new Timer(interval);
            _timer.Elapsed += (sender, args) => Elapsed?.Invoke(sender, args);
        }

        public event ElapsedEventHandler Elapsed;

        public void Dispose() => _timer.Dispose();

        public void Start() => _timer.Start();

        public void Stop() => _timer.Stop();
    }
}