﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        private static string _idRegex = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"; // XX-YYYY-XX
        public static string IdRegex { get => _idRegex; set => _idRegex = new Regex(value).ToString(); }

        public static int ParkingSize { get; set; } = 10;

        public static decimal ParkingBalance { get; set; } = 0;

        public static double ChargePeriod { get; set; } = 5 * 1000; // in milisecinds

        public static double LoggingPeriod { get; set; } = 60 * 1000; // in milisecinds

        public static decimal PenaltyRate { get; set; } = 2.5m;

        public static IDictionary<VehicleType, decimal> Rates { get; } = new Dictionary<VehicleType, decimal>()
        {
            { VehicleType.Bus, 3.5m},
            { VehicleType.Motorcycle, 1},
            { VehicleType.PassengerCar, 2},
            { VehicleType.Truck, 5}
        };
    }
}