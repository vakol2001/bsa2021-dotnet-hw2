﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static bool _isExiting = false;
        private static Parking _instanse;
        public static Parking Instance
        {
            get
            {
                if (!_isExiting)
                {
                    _instanse = new Parking();
                    _isExiting = true;
                }
                return _instanse;
            }
        }
        private Parking() { }

        public decimal Balance { get; internal set; }

        public int Capasity { get; } = Settings.ParkingSize;
        public IList<Vehicle> Vehicles { get; } = new List<Vehicle>(Settings.ParkingSize);

    }
}
