﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using CoolParking.BL.Helpers;
using RandomDataGenerator.FieldOptions;
using RandomDataGenerator.Randomizers;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {

        public static string IdRedex { get; } = Settings.IdRegex;
        private static readonly IRandomizerString _randomizerIdRegex = RandomizerFactory.GetRandomizer(new FieldOptionsTextRegex { Pattern = IdRedex });

        public static string GenerateRandomRegistrationPlateNumber()
        {
            return _randomizerIdRegex.Generate();
        }

        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            ValidateId(id);
            ValidateVehicleType(vehicleType);
            ValidateBalance(balance);
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
        private static void ValidateId(string id)
        {
            if (id is null)
            {
                throw new ArgumentNullException(nameof(id));
            }
            if (!Regex.IsMatch(id, IdRedex))
            {
                throw new ArgumentException($"Incorrect format of {nameof(id)}");
            }
        }
        private static void ValidateVehicleType(VehicleType vehicleType)
        {
            if (!EnumHelper.IsDefined(vehicleType))
            {
                throw new ArgumentException($"{nameof(vehicleType)} contains an invalid value");
            }
        }
        private static void ValidateBalance(decimal balance)
        {
            if (balance < 0)
            {
                throw new ArgumentException($"{nameof(balance)} can't be negative");
            }
        }




    }
}