﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public TransactionInfo(decimal sum, string vehicleId)
        {
            Sum = sum;
            VehicleId = vehicleId;
            TransactionDate = DateTime.Now;
        }

        public decimal Sum { get; }
        public string VehicleId { get; }
        public DateTime TransactionDate { get; }
        public override string ToString()
        {
            return $"[{TransactionDate.ToShortDateString(),10} {TransactionDate.ToLongTimeString(),8}] {Sum,6} c.u. was debited from the account of the {VehicleId,10} vehicle.";
        }
    }
}