﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Helpers
{
    public static class EnumHelper
    {
        public static bool IsDefined<T>(T enumValue)
        {
            if (typeof(T).BaseType != typeof(Enum)) throw new ArgumentException($"{nameof(T)} must be an enum type.");

            return EnumValueCache<T>.DefinedValues.Contains(enumValue);
        }
    }
    internal static class EnumValueCache<T>
    {
        public static HashSet<T> DefinedValues { get; }

        static EnumValueCache()
        {
            if (typeof(T).BaseType != typeof(Enum)) throw new Exception($"{nameof(T)} must be an enum type.");

            DefinedValues = new HashSet<T>((T[])Enum.GetValues(typeof(T)));
        }
    }
}
